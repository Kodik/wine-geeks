Rails.application.routes.draw do
  root to: 'home#index'

  get '/login_form', to: 'authentication#login_form'
  get '/register_form', to: 'authentication#register_form'
  post '/login', to: 'authentication#login'
  post '/register', to: 'authentication#register'
  get '/logout', to: 'authentication#logout'

  resources :vineries, only: %i[index show]
  resources :wines, only: %i[index show]
  namespace :admin do
    get '/dashboard', to: 'dashboard#show'
    resources :users, only: %i[index]
    resources :wines, except: :show
    resources :vineries, except: :show
  end
end
