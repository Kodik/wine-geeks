class Wine < ApplicationRecord
  belongs_to :vinery

  scope :filter_by_phrase, ->(phrase) { where('name ILIKE ?', "%#{phrase}%") if phrase.present? }

  enum color: %w[red rose white]
  enum sweetness: %w[dry semi_dry semi_sweet sweet]

  validates :name, presence: true
end
