class Vinery < ApplicationRecord
  has_many :wines

  scope :filter_by_phrase, ->(phrase) { where('name ILIKE ?', "%#{phrase}%") if phrase.present? }

  validates :name, presence: true
end
