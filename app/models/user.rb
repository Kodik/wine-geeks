class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :name, presence: true, uniqueness: true
  validates :password,
            length: { minimum: 6 },
            if: -> { new_record? || !password.nil? }

  scope :filter_by_phrase, ->(phrase) {
    where('name ILIKE ? OR email ILIKE ?', "%#{phrase}%", "%#{phrase}%") if phrase.present?
  }

  enum role: {
    user: 0,
    admin: 1
  }
end
