class Admin::VineriesController < Admin::AdminsController
  def index
    @vineries = Vinery.filter_by_phrase(search_params&.[](:phrase)).paginate(page: params[:page])
  end

  def edit
    @vinery = Vinery.find(params[:id])
  end

  def new
    @vinery = Vinery.new
  end

  def create
    @vinery = Vinery.new(vinery_params)
    if @vinery.save
      redirect_to controller: 'admin/vineries', action: 'index'
    else
      redirect_to controller: 'admin/vineries', action: 'new'
    end
  end

  def update
    @vinery = Vinery.find(params[:id])
    if @vinery.update(vinery_params)
      redirect_to controller: 'admin/vineries', action: 'index'
    else
      redirect_to controller: 'admin/vineries', action: 'edit'
    end
  end

  def destroy
    Vinery.find(params[:id]).destroy!
    redirect_to controller: 'admin/vineries', action: 'index'
  end

  private

  def vinery_params
    params.require(:vinery).permit(
      :name, :foundation_year, :image_url, :description, :contact, :location, :existing
    )
  end
end
