class Admin::AdminsController < ApplicationController
  layout 'admin'

  before_action :authorize!

  private

  def authorize!
    redirect_to controller: '/home', action: :index unless @current_user && @current_user.admin?
  end
end
