class Admin::WinesController < Admin::AdminsController
  def index
    @wines = Wine.filter_by_phrase(search_params&.[](:phrase)).paginate(page: params[:page])
  end

  def edit
    @wine = Wine.find(params[:id])
  end

  def new
    @wine = Wine.new
  end

  def create
    @wine = Wine.new(wine_params)
    if @wine.save
      redirect_to controller: 'admin/wines', action: 'index'
    else
      redirect_to controller: 'admin/wines', action: 'new'
    end
  end

  def update
    @wine = Wine.find(params[:id])
    if @wine.update(wine_params)
      redirect_to controller: 'admin/wines', action: 'index'
    else
      redirect_to controller: 'admin/wines', action: 'edit'
    end
  end

  def destroy
    Wine.find(params[:id]).destroy!
    redirect_to controller: 'admin/wines', action: 'index'
  end

  private

  def wine_params
    permitted = params.require(:wine).permit(
      :name, :vinery_id, :color, :sweetness, :alcohol_percent, :image_url, :description
    )
    permitted[:color] = params[:color].to_i
    permitted[:sweetness] = params[:sweetness].to_i
    permitted
  end
end
