class Admin::UsersController < Admin::AdminsController
  def index
    @users = User.filter_by_phrase(search_params&.[](:phrase)).paginate(page: params[:page])
  end
end
