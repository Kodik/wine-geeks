class ApplicationController < ActionController::Base
  before_action :set_current_user

  private

  def set_current_user
    token = cookies[:jwt_token]
    begin
      decoded = JsonWebToken.decode(token)
      @current_user = User.find_by(id: decoded[:user_id])
    rescue JWT::DecodeError => e
      @current_user =  nil
    end
  end

  def search_params
    params[:search]&.permit(:phrase)
  end
end
