class WinesController < ApplicationController
  def index
    @wines = Wine.includes(:vinery).filter_by_phrase(search_params&.[](:phrase)).
      paginate(page: params[:page])
  end

  def show
    @wine = Wine.find_by_id(params[:id])
    return redirect_to controller: :wines, action: :index unless @wine
  end
end
