class VineriesController < ApplicationController
  def index
    @vineries = Vinery.filter_by_phrase(search_params&.[](:phrase)).paginate(page: params[:page])
  end

  def show
    @vinery = Vinery.find_by_id(params[:id])
    return redirect_to controller: :vineries, action: :index unless @vinery
  end
end
