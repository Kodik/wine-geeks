class AuthenticationController < ApplicationController
  def login
    @user = User.find_by(email: login_params[:email])
    if @user&.authenticate(login_params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      cookies[:jwt_token] = token
      redirect_to controller: :home, action: :index
    else
      @user ||= User.new
      @user.errors.add(:email, 'Użytkownik z tym emailem nie istnieje') if @user.new_record?
      @user.errors.add(:password, 'Hasło jest nieprawidłowe') unless @user.new_record?
      render :login_form
    end
  end

  def login_form
    @user = User.new
  end

  def register
    @user = User.new(register_params)
    if @user.save
      render :login_form
    else
      render :register_form
    end
  end

  def register_form
    @user = User.new
  end

  def logout
    cookies.delete(:jwt_token)
    redirect_to controller: :home, action: :index
  end

  private

  def login_params
    params[:user].permit(:email, :password)
  end

  def register_params
    params[:user].permit(:name, :email, :password)
  end
end
