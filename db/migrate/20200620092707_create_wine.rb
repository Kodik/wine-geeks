class CreateWine < ActiveRecord::Migration[6.0]
  def change
    create_table :wines, id: :uuid do |t|
      t.references :vinery, foreign_key: true, type: :uuid
      t.string :name
      t.integer :color
      t.integer :sweetness
      t.integer :alcohol_percent
      t.string :image_url
      t.text :description

      t.timestamps
    end
  end
end
