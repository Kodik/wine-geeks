class CreateVineries < ActiveRecord::Migration[6.0]
  def change
    create_table :vineries, id: :uuid do |t|
      t.string :name
      t.string :foundation_year
      t.text :description
      t.string :contact
      t.string :location
      t.boolean :existing
      t.string :image_url

      t.timestamps
    end
  end
end
