User.create!(
  name: 'Admin',
  email: 'admin@example.com',
  password: 'Zaqw123',
  role: :admin
) unless User.exists?(name: 'Admin')

User.create!(
  name: 'Annula',
  email: 'anna.jeziorska@example.com',
  password: 'Zaqw123'
) unless User.exists?(name: 'Annula')

User.create!(
  name: 'Mirekk4',
  email: 'mirekk4k@example.com',
  password: 'Zaqw123'
) unless User.exists?(name: 'Mirekk4')

User.create!(
  name: 'Slowmo',
  email: 'slowmotion@gmail.com',
  password: 'Zaqw123'
) unless User.exists?(name: 'Slowmo')

User.create!(
  name: 'SieroDoGierro',
  email: 'sieminkiewicz@wp.pl',
  password: 'Zaqw123'
) unless User.exists?(name: 'SieroDoGierro')

User.create!(
  name: 'TheOne',
  email: 'noname@o2.pl',
  password: 'Zaqw123'
) unless User.exists?(name: 'TheOne')

User.create!(
  name: 'Spiderman',
  email: 'spiderman00@gmail.com',
  password: 'Zaqw123'
) unless User.exists?(name: 'Spiderman')

Vinery.create!(
  name: 'Luca Bosio Vineyards',
  foundation_year: '1967',
  contact: '+39 0141 847149',
  location: '10 - 12058 Santo Stefano Belbo (Włochy)',
  existing: true,
  image_url: 'https://domwina.pl/data/include/img/news/1523369954.jpg'
) unless Vinery.exists?(name: 'Luca Bosio Vineyards')

vinery = Vinery.find_by_name('Luca Bosio Vineyards')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Barolo',
  color: :red,
  sweetness: :dry,
  alcohol_percent: 14,
  image_url: 'https://sklep-wina.pl/pol_pl_Luca-Bosio-Barolo-271_1.png'
) unless Wine.exists?(name: 'Barolo')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Barbaresco',
  color: :red,
  sweetness: :dry,
  alcohol_percent: 14,
  image_url: 'https://winelistaustralia.com.au/bottles/ItalyLucaBosioBarbaresco.png'
) unless Wine.exists?(name: 'Barbaresco')

Vinery.create!(
  name: 'Bodegas Santalba',
  foundation_year: '1964',
  contact: '+34 941 304 231, santalba@santalba.com',
  location: 'Avda. de La Rioja S/N Gimileo La Rioja (Hiszpania)',
  existing: true,
  image_url: 'https://domwina.pl/data/include/cms/producers/Bodegas-Santalba_foto_producent2.png'
) unless Vinery.exists?(name: 'Bodegas Santalba')

vinery = Vinery.find_by_name('Bodegas Santalba')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Rioja Ogga',
  color: :red,
  sweetness: :dry,
  alcohol_percent: 14,
  image_url: 'https://i.pinimg.com/236x/82/db/15/82db15e39d33171efc08ac010a04b7b8.jpg'
) unless Wine.exists?(name: 'Rioja Ogga')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Vina Hermosa Blanco',
  color: :white,
  sweetness: :dry,
  image_url: 'https://domwina.pl/pol_pl_Vina-Hermosa-Blanco-411_1.png'
) unless Wine.exists?(name: 'Vina Hermosa Blanco')

Vinery.create!(
  name: 'Chateau Lestevenie',
  contact: '+33 6 48 62 23 73',
  location: '24240 Gageac-et-Rouillac (Francja)',
  existing: true,
  image_url: 'https://cdt24.media.tourinsoft.eu/upload/autour-saussignac-chateau-lestevenie--33--2.jpg'
) unless Vinery.exists?(name: 'Chateau Lestevenie')

vinery = Vinery.find_by_name('Chateau Lestevenie')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Bergerac Rose',
  color: :rose,
  sweetness: :dry,
  image_url: 'https://www.wine-searcher.com/images/labels/89/89/10728989.jpg'
) unless Wine.exists?(name: 'Bergerac Rose')

Vinery.create!(
  name: 'Stara Winna Góra',
  foundation_year: '1997',
  contact: '+48 68 385 92 79, santalba@santalba.com',
  location: 'Górzykowo 22, 66-131 Cigacice (Polska)',
  existing: true,
  image_url: 'https://media-cdn.tripadvisor.com/media/photo-s/07/a7/02/04/winny-dworek.jpg'
) unless Vinery.exists?(name: 'Stara Winna Góra')

vinery = Vinery.find_by_name('Stara Winna Góra')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Riesling Liryczny',
  color: :white,
  sweetness: :dry,
  alcohol_percent: 11,
  image_url: 'https://winezja.pl/zdjecia/riesling-liryczny-winna-gora_p_z.jpg'
) unless Wine.exists?(name: 'Riesling Liryczny')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Riesling znad Pradoliny',
  color: :white,
  sweetness: :semi_dry,
  alcohol_percent: 11,
  image_url: 'https://www.marekkondrat.pl/wp-content/uploads/images/2017/09/4810-Stara-Winna-Gora-Riesling-Wybor-z-Gron-2016-front-small.jpg'
) unless Wine.exists?(name: 'Riesling znad Pradoliny')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Regencja',
  color: :red,
  sweetness: :dry,
  alcohol_percent: 12,
  image_url: 'http://www.polskiewina.com.pl/userdata/public/gfx/e374b38177165a6ffd02b9efb026eb88.jpg'
) unless Wine.exists?(name: 'Regencja')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Regent',
  color: :red,
  sweetness: :dry,
  alcohol_percent: 12,
  image_url: 'https://studiowina.pl/wp-content/uploads/2016/07/krojcig12.jpeg'
) unless Wine.exists?(name: 'Regent')

Vinery.create!(
  name: 'Winnica Turnau',
  foundation_year: '2010',
  contact: '+48 91 307 91 31 t.pilarski@winnicaturnau.pl',
  location: 'Baniewice 115, 74-110 Banie (Polska)',
  existing: true,
  image_url: 'https://winicjatywa.pl/wp-content/uploads/2016/06/banie_zp_pl_winiarnia.jpg'
) unless Vinery.exists?(name: 'Winnica Turnau')

vinery = Vinery.find_by_name('Winnica Turnau')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Solaris',
  color: :white,
  sweetness: :dry,
  alcohol_percent: 13,
  image_url: 'https://sklep-wina.pl/pol_pl_Solaris-Winnica-Turnau-329_1.png'
) unless Wine.exists?(name: 'Solaris')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Rose',
  color: :rose,
  sweetness: :dry,
  alcohol_percent: 10,
  image_url: 'https://domwina.pl/pol_pl_Rose-Winnica-Turnau-206_2.png'
) unless Wine.exists?(name: 'Rose')
Wine.create!(
  vinery_id: vinery.id,
  name: 'Cabernet',
  color: :red,
  sweetness: :dry,
  alcohol_percent: 13,
  image_url: 'https://domwina.pl/pol_pl_Cabernet-Winnica-Turnau-59_2.png'
) unless Wine.exists?(name: 'Cabernet')
